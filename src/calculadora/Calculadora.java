package calculadora;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Luis Alberto Andia Mamani
 */
public class Calculadora {

    public double suma(double a, double b) {
        return a + b;
    }

    public double resta(double a, double b) {
        return a - b;
    }

    public double multiplicacion(double a, double b) {
        return a * b;
    }

    public double division(double a, double b) {
        if (b == 0) {
            System.out.println("division por cero");
            return -1;
        }
        return a / b;
    }

    public void menu() {
        System.out.println("SUMA [S]");
        System.out.println("RESTA [R]");
        System.out.println("DIVISION [D]");
        System.out.println("MULTIPLICACION [M]");
        System.out.println("Escribe una opcion:");
    }

    public static void main(String[] args) throws IOException {

        Calculadora calc = new Calculadora();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        calc.menu();
        String val = br.readLine();

        try {
            System.out.print("Ingresa primer valor: ");
            double a = Double.parseDouble(br.readLine());
            System.out.print("\nIngresa segundo valor: ");
            double b = Double.parseDouble(br.readLine());
            switch (val.toString()) {
                case "s":
                case "S":
                    System.out.println("La suma es: ");
                    System.out.println( calc.suma(a, b));
                    break;
                case "r":
                case "R":
                    System.out.println("la resta es:");
                    System.out.println(calc.resta(a, b));
                    break;
                case "m":
                case "M":
                    System.out.println("La multiplicacion es:");
                    System.out.println(calc.multiplicacion(a, b));
                    break;
                case "d":
                case "D":
                    System.out.println("La division es: ");
                    System.out.println(calc.division(a, b));
                    break;
            }

        } catch (NumberFormatException nfe) {
            System.err.println("Invalid Format!");
        }

    }

}
